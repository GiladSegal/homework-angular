// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/31023/slim/',
  firebase:{
    apiKey: "AIzaSyBNT3zB1rt_3YokXs6BdNniWx4DrJ8GFC8",
    authDomain: "messages-9d6eb.firebaseapp.com",
    databaseURL: "https://messages-9d6eb.firebaseio.com",
    projectId: "messages-9d6eb",
    storageBucket: "messages-9d6eb.appspot.com",
    messagingSenderId: "94538242178"
  }
};
