import { Injectable } from '@angular/core';
import{Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import {AngularFireDatabase} from 'angularfire2/database';
import { environment } from '../../environments/environment';

@Injectable()
export class MessagesService {
  http:Http;//http -> שם התכונה. Http-> סוג התכונה
  db:AngularFireDatabase;
  getMessages(){
      //return ['Message1','Message2','Message3','Message4'];
    //get messages from the SLIM rest API(DONT say DB!)
    return this.http.get(environment.url+"messages");

  }
  
  getMessage(id){
    return this.http.get(environment.url+'messages/' + id);
  }

  postMessage(data) // דטה= נתונים של הטופס
  {
    let options = { // הגדרנו דרך המחלקה של אנגולר האדר , רכיב קונטנט טייפ בישביל שהנתונים יעברו
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
   let  params = new HttpParams().append('message', data.message).append('user_id',data.user_id);
   //console.log(data) // פאראמס הוא מבנה נתונים המחזיק מפתח וערך 
   return this.http.post(environment.url+'messages', params.toString(),options);

  }

    putMessage(data,key){
        let options = {
          headers: new Headers({
            'content-type':'application/x-www-form-urlencoded'
          })
   
        }
        let params = new HttpParams().append('message',data.message);
        return this.http.put(environment.url+'messages/'+ key,params.toString(), options);
      }

  deleteMessage(key){
    return this.http.delete(environment.url+'messages/'+key);
  }

  getMessagesFire(){
    return this.db.list('/messages').valueChanges();
  }

  constructor(http:Http, db:AngularFireDatabase) {
    this.http = http;
    this.db =db;
   }
   

}
