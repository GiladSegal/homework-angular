import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import { FormsModule,ReactiveFormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessagesService } from "./messages/messages.service";
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { NavigationComponent } from './navigation/navigation.component';
import { MessagesFormComponent } from './messages/messages-form/messages-form.component';
import { MessageComponent } from './messages/message/message.component';
import { MessageFormComponent } from './messages/message-form/message-form.component';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { UpdateUsersComponent } from './users/update-users/update-users.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule} from "angularfire2/database";
import {environment} from './../environments/environment';
import { MessagesfirebaseComponent } from './messagesfirebase/messagesfirebase.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';


@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    UsersComponent,
    NavigationComponent,
    MessagesFormComponent,
    MessageComponent,
    MessageFormComponent,
    UsersFormComponent,
    UpdateUsersComponent,
    NotFoundComponent,
    MessagesfirebaseComponent,
    LoginComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([ // בניה של הראוטס
      {pathMatch:'full',path:'',component:MessagesComponent}, // הראוט הראשון זה דף הבית לוקלהוסט:4200
      {pathMatch:'full',path:'users', component:UsersComponent},
      {pathMatch:'full',path: 'message/:id', component: MessageComponent},
      {pathMatch:'full',path:'messagesForm/:key', component:MessagesFormComponent},
      {pathMatch:'full',path:'usersForm/:key', component:UsersFormComponent},
      {pathMatch: 'full',path: 'message-form/:id', component: MessageFormComponent},
      {pathMatch: 'full',path: 'update-users/:id', component: UpdateUsersComponent},
      {pathMatch: 'full',path: 'messagesfirebase', component: MessagesfirebaseComponent},
      {pathMatch: 'full',path: 'login', component: LoginComponent},
      
      
      
     // {path:'message/:id', component:MessageComponent},
      {path:'**',component:NotFoundComponent}// צריך להופיע אחרון כי הוא תופס את כל מה שלא מוגדר ומעביר אליו

    ])
  ],
  providers: [
    MessagesService,
    UsersService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
