import { Component, OnInit,Output , EventEmitter } from '@angular/core';
import { UsersService } from './../users.service';
import {FormGroup , FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'update-users',
  templateUrl: './update-users.component.html',
  styleUrls: ['./update-users.component.css']
})



export class UpdateUsersComponent implements OnInit {

    @Output() addUser:EventEmitter<any> = new EventEmitter<any>(); //any מגדיר סוג מסוים של מידע, במקרה הזה כל סוג
    @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>(); //אירוע פסימי

      service:UsersService;
      //Reactive Form
      //מתאים בדיוק לטופס עצמו
      usrform = new FormGroup({
          name:new FormControl(),
          email:new FormControl()
      });
  constructor(private route: ActivatedRoute ,service: UsersService) { 
    this.service = service;
  }

    sendData() {
        //הפליטה של העדכון לאב
      this.addUser.emit(this.usrform.value.name);
      console.log(this.usrform.value);
      this.route.paramMap.subscribe(params=>{
        let id = params.get('id');
        this.service.putUser(this.usrform.value, id).subscribe(
          response => {
            console.log(response.json());
            this.addUserPs.emit();
          }
        );
      })
    }
    user;


  ngOnInit() {

       this.route.paramMap.subscribe(params=>{
           let id = params.get('id');
           console.log(id);
           this.service.getUser(id).subscribe(response=>{
             this.user = response.json();
             console.log(this.user);
            })
        })
   }
            
}