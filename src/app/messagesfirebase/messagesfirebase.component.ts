import { Component, OnInit } from '@angular/core';
import { MessagesService } from '../messages/messages.service';


@Component({
  selector: 'app-messagesfirebase',
  templateUrl: './messagesfirebase.component.html',
  styleUrls: ['./messagesfirebase.component.css']
})
export class MessagesfirebaseComponent implements OnInit {
messages;
  
constructor(private service:MessagesService) { }

  ngOnInit() {
    this.service.getMessagesFire().subscribe(response=>
    {
      console.log(response);
      this.messages=response;
    })
  }

}
