import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesfirebaseComponent } from './messagesfirebase.component';

describe('MessagesfirebaseComponent', () => {
  let component: MessagesfirebaseComponent;
  let fixture: ComponentFixture<MessagesfirebaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesfirebaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesfirebaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
